async function getCurrentUser() {
	return fetch('/api/CurrentUser')
		.then(response => {
			// check if the response status is ok (200)
			if (!response.ok) {
				if (response.status === 401) {
					// Handle 401 Unauthorized here
					console.log('Unauthorized');
					return {"status": 401};
				} else {
					throw new Error('Network response was not ok');
				}
			}

			// Parse the JSON response
			return response.json();
		}
		)
		.catch(error => {
			// Handle any errors that occurred during the fetch
			console.error('Fetch error:', error);
		})
}
