use std::env;
use env_logger;

use actix_session::{SessionMiddleware, storage::RedisSessionStore};
use actix_web::{App,web, HttpServer, middleware};
use actix_web::cookie::Key;
use sqlx::PgPool;

mod handlers;
pub mod data;
pub mod db;
use data::AppContext;


use handlers::*;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let con_string=env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let db_pool = PgPool::connect(con_string.as_str()).await
        .expect("Failed to connect to the database");
    
    let redis_con_string=env::var("REDIS_URL").expect("REDIS_URL must be set");
    let store = RedisSessionStore::new(redis_con_string).await.unwrap();

    let secret_key: Key=Key::from(env::var("SECRET_KEY").expect("SECRET_KEY must be set").as_bytes());
    HttpServer::new(move ||
        App::new()
            .app_data(
                web::Data::new(AppContext {
                    db_pool: db_pool.clone()
                })
            )
            .wrap(middleware::Logger::default())
            .wrap(
                SessionMiddleware::new(
                    store.clone(),
                    secret_key.clone()
                    )
                )
            .service(index)
            .service(register_user)
            .service(login_user)
            .service(logout_user)
            .service(get_current_user)
    )
    .bind("0.0.0.0:3000")?
    .run()
    .await
}
