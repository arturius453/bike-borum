use std::fmt;
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::PgPool;
use serde::{Deserialize, Serialize};

pub struct AppContext {
    pub db_pool: PgPool,
}
#[derive(sqlx::Type, Debug)]
#[derive(Serialize, Deserialize)]
#[sqlx(type_name="role",rename_all = "snake_case")]
pub enum Role{
    Admin,
    User
}
impl fmt::Display for Role {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Role::Admin => write!(f, "admin"),
            Role::User => write!(f, "user"),
        }
    }
}
#[derive(serde::Deserialize)]
pub struct UserForm {
   pub username: String,
   pub email: String,
   pub password: String,
}
#[derive(serde::Deserialize)]
pub struct Thread {
   pub title: String,
   pub body: String,
}

#[derive(sqlx::Type, Debug)]
#[derive(Deserialize, Serialize)]
pub struct User {
   pub id: i32,
   pub username: String,
   pub email: String,
   #[serde(skip_serializing)]
   pub password: String,
   pub created_at: DateTime<Utc>,
   pub role: Role,
}

