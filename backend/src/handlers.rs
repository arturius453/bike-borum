use super::db::*;

use super::data::*;
use actix_session::Session;
use actix_web::{get, post, web, HttpResponse};
use bcrypt::{hash, verify, DEFAULT_COST};
use log::debug;

#[get("/api/")]
async fn index() -> HttpResponse {
    HttpResponse::Ok().body("API is running!")
}

#[get("/api/CurrentUser")]
async fn get_current_user(session: Session, app_data: web::Data<AppContext>) -> HttpResponse {
    let user_id: i32;
    match session.get::<i32>("user_id").unwrap() {
        Some(id) => user_id = id,
        None => return HttpResponse::Unauthorized().finish()  
    }
    let pool=&app_data.db_pool;
    let user;
    match get_user_by_id(user_id,pool).await {
        Ok(u) => user = u,
        Err(e) => return HttpResponse::InternalServerError().body(e.to_string()),
    }
    debug!("User: {:?} ", user);

    let respone=serde_json::to_string(&user).unwrap();

    return HttpResponse::Ok().body(respone.to_string());
}

#[post("/api/register")]
async fn register_user(data: web::Form<UserForm>, app_data: web::Data<AppContext>) -> HttpResponse {
    let pool = &app_data.db_pool;

    let hashed_password = hash(data.password.clone(), DEFAULT_COST).ok();

    let new_user;
    match sqlx::query!(
        "INSERT INTO users (username, email, password) VALUES ($1, $2, $3) RETURNING id",
        data.username,
        data.email,
        hashed_password
    )
    .fetch_one(pool)
    .await
    {
        Ok(user) => new_user = user,
        Err(e) => return HttpResponse::InternalServerError().body(e.to_string()),
    }

    debug!("New user id: {}", new_user.id);
    HttpResponse::Found()
        .append_header(("Location", "/login"))
        .finish()
}

#[post("/api/login")]
async fn login_user(
    data: web::Form<UserForm>,
    session: Session,
    app_data: web::Data<AppContext>,
) -> HttpResponse {
    let pool = &app_data.db_pool;
    debug!("Login {}: {}", data.username, data.password);

    let result;
    match sqlx::query!(
        "SELECT id, password FROM users WHERE username = $1",
        data.username
    )
    .fetch_one(pool)
    .await
    {
        Ok(res) => result = res,
        Err(e) => match e {
            sqlx::Error::RowNotFound => {
                return HttpResponse::Found()
                    .append_header(
                        ("Location", "/login?erorr=user_not_found"))
                    .finish()
            }
            _ => return HttpResponse::InternalServerError()
                .body(e.to_string()),
        },
    }
    if verify(data.password.clone(),&result.password).unwrap()
    {
        session.insert("user_id", result.id).unwrap();
        return HttpResponse::Found()
            .append_header(("Location", "/"))
            .finish();
    } else {
        return HttpResponse::Unauthorized()
            .body("Invalid username or password");
    }
}

#[post("/api/logout")]
async fn logout_user(session: Session) -> HttpResponse {
    session.remove("user_id");
    HttpResponse::Found()
        .append_header(("Location", "/"))
        .finish()
}

#[post("/api/create-thread")]
async fn create_thread(
    data: web::Form<Thread>,
    session: Session,
    app_data: web::Data<AppContext>,
) -> HttpResponse {
    let pool = &app_data.db_pool;
    let user_id: i32;
    match session.get::<i32>("user_id").unwrap() {
        Some(id) => user_id = id,
        None => return HttpResponse::Unauthorized().finish()  
    }
    match is_admin(user_id, pool).await {
        Ok(is_admin) => {
            if !is_admin {
                return HttpResponse::Forbidden().finish();
            }
            
        }
        Err(e) => return HttpResponse::InternalServerError().
            body(e.to_string()),
    }
    let new_thread_id;
    match sqlx::query!(
        "INSERT INTO threads (title ) VALUES ($1) RETURNING id",
        data.title,
    )
    .fetch_one(pool)
    .await
    {
        Ok(thread) => new_thread_id = thread.id,
        Err(e) => return HttpResponse::InternalServerError().body(e.to_string()),
    }
    return HttpResponse::Found()
        .append_header(("Location", format!("/thread/{}", new_thread_id)))
        .finish();
}
