use super::data::User;
use super::data::Role;
use sqlx::PgPool;

pub async fn get_user_by_id(id: i32, pool: &PgPool) -> Result<User, sqlx::Error> {
    sqlx::query_as!(
        User,
        "SELECT
            id,
            username,
            created_at,
            email,
            password,
            role AS \"role:Role\"
        FROM users 
        WHERE id = $1", id)
        .fetch_one(pool)
        .await
}

pub async fn is_admin(user_id: i32, pool: &PgPool) -> Result<bool, sqlx::Error> {
    match sqlx::query!(
        "SELECT (role = 'admin') AS is_admin
        FROM users
        WHERE id = $1", user_id
    ).fetch_one(pool)
    .await {
        Ok(res) => {println!("{:?}", res); Ok(true)},
        Err(e) => return Err(e)
    }
}
