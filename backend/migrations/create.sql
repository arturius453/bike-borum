CREATE TYPE role AS ENUM ('user', 'admin');
CREATE TABLE Users (
	id SERIAL PRIMARY KEY,
	username VARCHAR(255) NOT NULL UNIQUE,
	email VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	role role  NOT NULL DEFAULT 'user'
);
CREATE TABLE Threads (
  id SERIAL PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TABLE Posts (
  id SERIAL PRIMARY KEY,
  body TEXT NOT NULL,
  user_id INT NOT NULL,
  thread_id INT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  FOREIGN KEY (user_id) REFERENCES Users (id),
  FOREIGN KEY (thread_id) REFERENCES Threads (id)

);
CREATE INDEX threads_title_index ON Threads (title);
